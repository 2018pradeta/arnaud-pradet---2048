from game2048.grid_2048 import *

def read_player_command():
    move = str(input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):"))
    print(move)
    while move not in ["g","b","d","h"]:
        print("Commande invalide")
        move = str(input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):"))
    return move


def read_size_grid():
    size=int(input("Taille grille: "))
    while type(size)!=int or size<0:
        print("Commande invalide")
        size=input("Taille grille: ")
    return size


THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32:
"32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048",
4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8:
"Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048:
"Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8:
"C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K",
4096: "L", 8192: "M"}}

def read_theme_grid():
    num_theme=input("Themes (0:default, 1:chemistry, 2:alphabet) :")
    while num_theme not in '012':
        print("Commande invalide")
        num_theme=input("Themes (0:default, 1:chemistry, 2:alphabet) :")
    return THEMES[num_theme]

def game_play_nul():
    size=read_size_grid()
    theme=read_theme_grid()
    grid=init_game(size)
    print(grid_to_string_with_size_and_theme(grid,theme))
    over=False
    while not over:
        move_pos=move_possible_direction(grid)
        print(move_pos)
        di=read_player_command()
        if di in move_pos:
            grid=move_grid(grid,di)
            grid_add_new_tile(grid)
            print(grid_to_string_with_size_and_theme(grid,theme))
            over=is_game_over(grid)
    if get_grid_tile_max(grid)>=2048:
        print('Vous avez gagné')
    else:
        print('Vous avez perdu')


if __name__ == '__main__':
    game_play_nul()
    exit(1)
